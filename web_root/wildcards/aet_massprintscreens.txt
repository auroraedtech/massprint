<!DOCTYPE html>
<html>
<head>
	~[wc:commonscripts]
	<link href="/images/css/screen.css" rel="stylesheet" media="screen">
	<link href="/images/css/print.css" rel="stylesheet" media="print">
	<style>
		.studentscreen {
			page-break-after:always;
		}
		a.dialog img, a.dialogM img {
			visibility:visible;
		}
		div#content-main {
			margin-left:0px;
		}
		body {
			background:none !important;
		}
		input.sizeadjust {
			width:3em;
		}
		#btnContMax, #btnNoNav {
			display:none;
		}
	</style>
	<script>
		var font_adjustment = 90;

		var sent = 0;
		var completed = 0;
		var promises = [];
		var screens;

		var studpage = "~(gpv.studpage)" || "~(gpv.-studpage)";

		function load_batch(start, interval)
		{
			var promises = [];
			screens.slice(start, start+interval).each(function(i, screen) {
				var studentsdcid = $j(screen).attr("studentsdcid");
				promises.push(
					$j.ajax({
						"url": studpage + "&frn=001" + studentsdcid,
						"i": i,
						"studentsdcid": studentsdcid,
						"success": function(response) {
							{
								completed++;
								if(this.i == 0) $j("[studentsdcid='" + this.studentsdcid + "']").append( $j(response).filter("style,link") );

								var content = $j(response).find("#content-main");
								content.find("script").remove();
								~[if.~(gpv.removetitle)=1]content.find("h1 *,#page-heading-alerts").remove();[/if]

								$j("[studentsdcid='" + this.studentsdcid + "']").append( content );
								updateLoadingDialogPercentComplete(completed/~[x:studsinset]*100);
							}
						}
					})
				);
				sent++;
			});
			$j.when.apply($j, promises).then(function() {
				if(sent < screens.length)
				{
					load_batch(start+interval, interval);
				}
				if (completed >= screens.length)
				{
					updateLoadingDialogPercentComplete(100);
					closeLoading("aet_massprint");
					$j("[data-ng-cloak]").removeAttr("data-ng-cloak");
					window.print();
				}
			});
		}

		$j(document).ready(function()
			{
				loadingDialog("Loading", "aet_massprint");
				updateLoadingDialogPercentComplete(0);

				screens = $j("div.studentscreen");

				load_batch(0, 100)

				$j(".studentscreen :last").css("page-break-after", "avoid")
				$j("input.sizeadjust").click(function()
					{
						font_adjustment += parseInt($j(this).attr("factor"));
						$j(".studentscreen").css("font-size", (font_adjustment) + "%");
					}
				);
			}
		);

	</script>
	</head>
	<body>
		<div id="header" class="noprint" style="text-align:center; font-size:12pt;">
			<p><a href="javascript:window.print()"><input type="button" value="Print" /></a>&nbsp;<a href="/~[referer.short]"><input type="button" value="Back" /></a></p>
			<p><input type="button" class="sizeadjust" value="-" factor="-10"> Adjust font size <input type="button" class="sizeadjust" value="+" factor="10"></p>
		</div>
		<div id="screencontainer">
			~[tlist_sql;
				select
					students.dcid
				from
					students
					join ~[temp.table.current.selection:Students] temp on temp.dcid = students.dcid
				order by
					students.lastfirst
			;]
			<div studentsdcid="~(dcid)" class="studentscreen"></div>
			[/tlist_sql]
		</div>
	</body>
</html>